import sys
import time
import os
import csv

def get_files(dirpath,module,subtype,csvfile):
    rootdir = dirpath
    filename = csvfile
    lst_files=[]
    fin_lst=[]
    exclude = set(['logs'])
    fnames = ('.log','.out')
    for subdir, dirs, files in os.walk(rootdir,topdown=True):
        dirs[:] = [d for d in dirs if d not in exclude]
        for file in files:
            if not file.endswith(fnames):
                path = subdir
                fpath = os.path.join(subdir,file)
                epoch = os.path.getmtime(fpath)
                ts = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch))
                fin = module+","+subtype+","+file+","+path+","+ts
                lst_files.append(fin)
    for i in lst_files:
        l = i.split(',')
        fin_lst.append(l)

    with open(filename, 'a') as f:
        write = csv.writer(f)
        write.writerows(fin_lst)

#get_files('/hadoop/app/bdh/retrofit/interaction_depot','ID','Scripts','/hadoop/app/bdh/retrofit/data/informatica/infa_cmd.csv')

get_files(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
