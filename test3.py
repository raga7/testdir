##-----------UNIX Commands-------------####

cat filename -->display
vi filename + i ->insert; esc:wq! ->save; esc:q! ->Exit without save
ls - list of files
ls -lrth
ls -lrta
ls -lrt *.log|head -5 -> gives first 5 lines/logs
ls -ltr *.log|tail -5 -> gives last 5 records
ls -ltr |tail -4
grep 'logfile' pr_load_rft_teradata.py --->searches logfile in that py script
cp -R /hadoop/app/bdh/retrofit/scripts /hadoop/app/bdh/retrofit/Prod_Ready   ----Copy from one dir to other

##--- read data from CSV and store it in dataframe and then to hive table------- 

#execute it in pyspark

from pyspark import SparkConf
from pyspark.sql import SparkSession

spark = SparkSession.builder\
                .config(conf=conf)\
                .enableHiveSupport()\
                .getOrCreate()

df = spark.read.options(header='False', inferSchema='True', delimiter='|') \
     .csv("/app/bdh/retrofit/test/dml_test.csv")
  
df.write.mode("overwrite").saveAsTable("app_bdh_rft_raw.dml_test_t")
##------------------------##---------------------------##

--- Copy csv data from hadoop path into hive table------- 
try :
        df=spark.read.option("multiline", "true")\
        .option("quote", '"')\
        .option("header", "true")\
        .option("escape", "\\")\
        .option("escape", '"')\
		.option("timestampFormat", "MM-dd-yyyy hh mm ss")\
        .csv("/app/bdh/retrofit/data/Hive/hive_metadata.csv")
        logging.info("Successfully read input file")
except:
        print("Failed while reading input file")

##--------------------##-----------------------##

--- code to load data from hive table to teradata------------

df=spark.sql("""select * from app_bdh_rft_analytic.change_version_info_planned_instl_t""")

df.write \
    .format("jdbc") \
    .option("url", "jdbc:teradata://OSIUAT/DATABASE=R3_IDW_DATA,TMODE=ANSI,LOGMECH=LDAP") \
    .option("dbtable", "R3_IDW_DATA.RFT_Infa_test3") \
	.option("driver", "com.teradata.jdbc.TeraDriver") \
    .option("user", "svc.dart_ria_dev") \
    .option("password", "UOeqoY#BGw8i$uc") \
	.option("mode", "append") \
    .save()

-----------------------------------------

Create Table Syntax in Teradata:
----------------------------
CREATE SET TABLE IDW_DMG.CTT_Talend_User_Context, FALLBACK
(PROJECT_NAME varchar(100),
JOB_NAME varchar(100),
DATA_ASSET_NAME varchar(100),
KEY varchar(100),
VALUE varchar(100),
USER_NAME varchar(100),
create_ts timestamp,
load_ts timestamp);

===========================================================================================
# --------------------------------------------------------------------------------- #
# pr_rft_ctrlm.py
#
# Developed by: Ashwini Ninave
# Developed on: 03-02-2021
# Project :RIA
# Purpose: This script will fetch metadata tables from sql server tables
#          And load it into primary tables.
#
#
# Input:
# Output table(s)/files: app_bdh_ria_raw.ctm_job_t, app_bdh_ria_raw.ctm_job_predecessor_t, 
# --------------------------------------------------------------------------------- #

=========================================================================================
df_temp = df_task.dropDuplicates(['jobname','dep_date'])
df['jobname','lastdeploymentdate'].sort(desc("lastdeploymentdate")).show(10,False)
from pyspark.sql.functions import count, desc
df.groupBy("jobname").agg(count("jobname").sort(desc("lastdeploymentdate")).show(10,False)
df.registerTempTable("exec_task_table")
df_sorted=df.sort(df.lastdeploymentdate.desc())
df1 =df_sorted.filter(df_sorted.dep_date.like('2021-02-18'))
df1['lastdeploymentdate','jobscriptarchivefilename','dep_date'].show()
df_fin.filter(df_fin.task_id= "task_3187").show()
df_new = df.orderBy(['jobname','lastdeploymentdate'],ascending=False)
df_temp = df_task.dropDuplicates(['jobname','dep_date'])
df_new= df_task.sort("jobname","dep_date",ascending=False)
---------------------------------------------------

>>> df1=spark.sql("""SELECT * FROM app_bdh_rft_raw.bdh_hive_metadata_t_raw""" )
df2=spark.sql(""" select * from df1 where df1.filter(df1.db_name.like('%bdh%','%app%'))""").show()
>>> df2=df1.sort(df1.db_name.asc())
>>> df2.show()
df2[['date_time']].show()
===============================
row_list = df.select('idquartzjob').collect()
task_id = [ row.idquartzjob for row in row_list]
task_id ## holds all values for which we need to check in tac server
tasks = ['task_'+str(row.idquartzjob) for row in row_list]
tasks
-------------------------------
============================================================================================
CREATE TABLE table_csv_export_data
ROW FORMAT DELIMITED FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
STORED as textfile
AS
select
'id' as id
,'first_name' as first_name
,'last_name' as last_name
,'join_date' as join_date;


INSERT INTO table_csv_export_data
SELECT
 id
 ,first_name
 ,last_name
 ,join_date
FROM
 table_orc_data;
--------------------------------------------- 
 Query to find duplicate entries:	
------------------------------------------------------
spark.sql("""select *,count(*) from app_bdh_rft_analytic.change_version_teradata_dml_t  group by 
logdate,queryid ,statementgroup ,statementtype ,querytext ,ProcID ,CollectTimeStamp ,ZoneID ,
ObjectDatabaseName ,ObjectTableName ,ObjectColumnName ,ObjectID ,ObjectNum ,
ObjectType ,FreqofUse ,TypeofUse ,create_ts ,load_ts having count(*)>1""").count()
--------------------------------------------------------------
lst_dir_id=[]
lst_dir_id = os.listdir('/hadoop/app/bdh/marketing/interaction_depot')
for dir in lst_dir_id:
	if os.path.isdir(dir):
		print('Path points to a Directory:')
====================================================
=====================================
import os
import time
import stat
lst = os.listdir(os.getcwd())
Obj = os.stat(os.getcwd())
for i in lst:
	ts = time.ctime ( Obj [ stat.ST_MTIME ] )
	#print('filename :{} {} '.format(i,ts))
	new_lst.append(i,ts)
print(new_lst)	
====================================================

import csv
with open('GFG', 'w') as f:
	write = csv.writer(f)
	write.writerow(tasks)


with open('GFG','r') as f:
	csvFile = csv.reader(f)
	for lines in csvFile:
		print(lines)
-----------------------------------------------------
=========================================
	
	spark.sql("""select regexp_extract((split(querytext,' ')[2]),'[.]([^.]+)',1) as Tabname
	from app_bdh_rft_analytic.change_version_teradata_dml_t 
	where statementtype like 'Delete%'""").show(1,False); --------------- DELETE
	
	spark.sql("""select split(querytext,'"')[3] as TabName
	from app_bdh_rft_analytic.change_version_teradata_dml_t 
	where statementtype like 'Insert%' """).show(1,False);
	spark.sql("select regexp_extract('foo.bar','[.]([^.]+)',1)").show() ------------ INSERT
	
	spark.sql("""select regexp_extract((split(querytext,' ')[1]),'[.]([^.]+)',1) as Tabname
	from app_bdh_rft_analytic.change_version_teradata_dml_t 
	where statementtype like 'Update%'""").show(1,False); ---------UPDATE
	
	-----------------------------------------------
	spark.sql("""
	select 
		statementtype,
		querytext,
        case 
            when trim(lower(STATEMENTTYPE)) LIKE 'update%'
		then regexp_extract((split(querytext,' ')[1]),'[.]([^.]+)',1) 
            when trim(lower(STATEMENTTYPE)) LIKE 'insert%' 
		then split(querytext,'"')[3] 
            when trim(lower(STATEMENTTYPE)) LIKE 'delete%' 
		then regexp_extract((split(querytext,' ')[2]),'[.]([^.]+)',1)
		else 'none'
        end as Tabname
		from app_bdh_rft_analytic.change_version_teradata_dml_t
		where statementtype like 'Delete%'""").show(10,False);
------------------------------------------------------------------
spark.sql("""select * from (SELECT
task_id,
zipfile_name,
date_time as job_date_time,
row_number() over (partition by task_id order by date_time desc ) rnk
from app_bdh_rft_stage.all_task_data ) ilv
where rnk = 1""").show()
-------------------------------------------------------------------------
